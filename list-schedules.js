import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './list-schedules-styles.js';
import '@cells-components/cells-icon';
/**
This component es visual y lista todos los recordatorios del usuario.

Example:

```html
<list-schedules></list-schedules>
```
* @customElement
* @demo demo/index.html
* @extends {LitElement}
*/
export class ListSchedules extends LitElement {
  static get is() {
    return 'list-schedules';
  }

  // Declare properties
  static get properties() {
    return {
      schedulesList: Array
    };
  }

  // Initialize properties
  constructor() {
    super();
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('list-schedules-shared-styles').cssText}
    `;
  }

  /**
   * Este método retorna un componente. 
   * @param {Object} detail Los detalles para mostrar
   */
  BlockTime(detail) {
    return html`
      <article class="block-time">
        <div class="head">
          <h2>${ detail.description}</h2>
          <h3> ${ detail.address} </h3>
        </div>
        <div class="body">
          <span class="hour">${ ('0' + detail.hour).slice(-2)}</span>
          <span>:</span>
          <span class="minutes">${ ('0' + detail.minutes).slice(-2)}</span>
        </div>
        <div class="footer">
          <button class="delete" @click="${ () => this._deleteSchedule(detail.id)}">
            <cells-icon icon="coronita:trash"></cells-icon>
          </button>
        </div>
      </article>
    `
  }

  /**
   * Este método se ejecuta cuando se presiona sobre el botón de borrar.
   * @param {Number} id Id del recordatorio
   */
  _deleteSchedule(id) {
    this.dispatchEvent(new CustomEvent('delete_schedule', { detail: { id }, composed: true, bubbles: true }));
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <h1>Estos son tus recordatorios</h1>
      
      ${ this.schedulesList ? this.schedulesList.map((schedule) => {
      return this.BlockTime(schedule);
    }) : ''}
      `;
  }
}

// Register the element with the browser
customElements.define(ListSchedules.is, ListSchedules);

import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --list-schedules; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

h1 {
  text-align: center;
  font-size: 21px;
  color: #616161; }

h3 {
  text-align: center;
  font-size: 16px;
  color: #6060b9; }

.block-time {
  width: 100%;
  padding: 15px;
  box-shadow: 0 2px 5px -2px #00000070, 0 0px 5px -2px #0000007d;
  width: 85%;
  margin: 0 auto;
  margin-bottom: 30px; }
  .block-time .head {
    margin-bottom: 10px; }
    .block-time .head h2, .block-time .head h3 {
      color: black;
      margin: 0; }
    .block-time .head h2 {
      font-size: 14px; }
    .block-time .head h3 {
      font-size: 10px;
      color: #696868;
      text-align: left;
      margin-top: 5px; }
  .block-time .body {
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
    justify-content: center; }
    .block-time .body span {
      font-size: 35px; }
    .block-time .body .hour, .block-time .body .minutes {
      font-size: 50px;
      padding: 0 20px; }
  .block-time .footer {
    text-align: right; }
    .block-time .footer button {
      background: none;
      border: none;
      padding: 8px 10px;
      box-shadow: 0 0 5px #ff8282 inset;
      border-radius: 50%; }
      .block-time .footer button cells-icon {
        color: #ff00007d;
        font-size: 8px;
        width: 20px; }
`;